﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Data.SqlClient;
using Dapper;
using Microsoft.Extensions.Configuration;
using MyBackendApp.Models;

namespace MyBackendApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Biodata")]
    public class BiodataController : Controller
    {
        private SqlConnection conn;
        private IConfiguration _config;
        private string strConn = string.Empty;

        public BiodataController(IConfiguration config)
        {
            _config = config;
            strConn = config.GetConnectionString("MyKoneksi");
        }

        // GET: api/Biodata
        [HttpGet]
        public IEnumerable<Biodata> Get()
        {
            using (conn = new SqlConnection(strConn))
            {
                string strSql = @"select * from Biodata  
                                  order by Nama asc";

                var hasil = conn.Query<Biodata>(strSql);
                return hasil;
            }
        }

        // GET: api/Biodata/5
        [HttpGet("{id}", Name = "Get")]
        public Biodata Get(int id)
        {
            string strSql = @"select * from Biodata 
                              where PersonID=@PersonID";

            var param = new { PersonID = id };

            using (conn = new SqlConnection(strConn))
            {
                var hasil = conn.QuerySingle<Biodata>(strSql, param);
                if (hasil != null)
                    return hasil;
                else
                    throw new Exception("Data tidak ditemukan");
            }
        }

        // POST: api/Biodata
        [HttpPost]
        public IActionResult Post([FromBody]Biodata biodata)
        {
            string strSql = @"insert into Biodata(Nama,Alamat,Email,Telp,Umur) 
                              values(@Nama,@Alamat,@Email,@Telp,@Umur)";
            var param = new
            {
                Nama = biodata.Nama,
                Alamat = biodata.Alamat,
                Email = biodata.Email,
                Telp = biodata.Telp,
                Umur = biodata.Umur
            };

            using (conn = new SqlConnection(strConn))
            {
                try
                {
                    conn.Execute(strSql, param);
                    return Ok("Data Biodata berhasil ditambah");
                }
                catch (SqlException sqlEx)
                {
                    return BadRequest(sqlEx.Message);
                }
            }
        }

        // PUT: api/Biodata/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Biodata biodata)
        {
            string strSql = @"update Biodata set Nama=@Nama,
                            Alamat=@Alamat,Email=@Email,Telp=@Telp,Umur=@Umur 
                            where PersonID=@PersonID";
            var param = new
            {
                Nama = biodata.Nama,
                Alamat = biodata.Alamat,
                Email = biodata.Email,
                Telp = biodata.Telp,
                Umur = biodata.Umur,
                PersonID = id
            };

            using (conn = new SqlConnection(strConn))
            {
                try
                {
                    conn.Execute(strSql, param);
                    return Ok("Data Biodata berhasil di update");
                }
                catch (SqlException sqlEx)
                {
                    return BadRequest(sqlEx.Message);
                }
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            string strSql = @"delete Biodata where PersonID=@PersonID";
            var param = new { PersonID = id };

            using (conn = new SqlConnection(strConn))
            {
                try
                {
                    conn.Execute(strSql, param);
                    return Ok("Data berhasil di delete !");
                }
                catch (SqlException sqlEx)
                {
                    return BadRequest("Error: " + sqlEx.Message);
                }
            }
                
        }
    }
}
